#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "lcl.h"
#import "lcl_config_components.template.h"
#import "lcl_config_extensions.template.h"
#import "lcl_config_logger.template.h"

FOUNDATION_EXPORT double LibComponentLogging_CoreVersionNumber;
FOUNDATION_EXPORT const unsigned char LibComponentLogging_CoreVersionString[];


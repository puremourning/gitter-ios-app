<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" indent="no"/>
<xsl:template match="/changeSet">
  Changes:
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="item">* <xsl:value-of select="comment"/>
</xsl:template>
</xsl:stylesheet>
